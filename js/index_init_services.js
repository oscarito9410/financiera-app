/*global data_support, intel */

/* --------------
 initialization vb 
   receives the data of the service method and can return alternate data
   thus you can reformat dates or names, remove or add entries, etc.
   -------------- */

/*global angular*/
angular.module('starter', ['ionic','uiGmapgoogle-maps', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}).config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "menu.html",
      controller:"ctrl"
    })
    
  
    .state('app.quien', {
      url: "/quien",
      views: {
        'menuContent' :{
          templateUrl: "quien.html"
        }
      }
    })

    .state('app.cotizar', {
      url: "/cotizar",
      views: {
        'menuContent' :{
          templateUrl: "cotizar.html",
          controller:"ctrlCotizador"
        }
      }
    })

    .state('app.sucursal', {
      url: "/sucursal",
      views: {
        'menuContent' :{
          templateUrl: "sucursal.html",
          controller:"ctrl"
        }
      }
    })
    .state('app.playlists', {
      url: "/playlists",
      views: {
        'menuContent' :{
          templateUrl: "playlists.html",
          controller: 'PlaylistsCtrl'
        }
      }
    })

    .state('app.single', {
      url: "/playlists/:playlistId",
      views: {
        'menuContent' :{
          templateUrl: "playlist.html",
          controller: 'PlaylistCtrl'
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/quien');
});


angular.module('starter.controllers', [])
.controller("ctrl",function($scope,$ionicLoading){
    $scope.dirUser={};
    
    $scope.mapa={};
    $scope.mapa.laltitud=19.3946117;
    $scope.mapa.longitud=-99.15397989999997;
    
    $scope.map = { center: { latitude:parseFloat($scope.mapa.laltitud), longitude: parseFloat($scope.mapa.longitud)}, zoom: 15 };
    
    navigator.geolocation.getCurrentPosition(function(position) {
        console.log(position);
        
        $scope.dirUser.laltitud=position.coords.latitude;
        $scope.dirUser.longitud=position.coords.longitude;    
        $scope.dirUser.cercania=$scope.getDistancia($scope.dirUser.laltitud,$scope.dirUser.longitud,
                                                    $scope.mapa.laltitud,$scope.mapa.longitud);
       $scope.dirUser.cercania=(parseFloat($scope.dirUser.cercania));
      console.log($scope.dirUser); 
       $scope.marker = {
            id: 0,
            coords: {
                latitude: $scope.mapa.laltitud,
                longitude: $scope.mapa.longitud
            },
            options: {
                draggable: false
            }
        };
        
        
    });
    
    $scope.getDistancia=function(laltitud,longitud,laltitud_dos,longitud_dos){
          var p = 0.017453292519943295;    // Math.PI / 180
          var c = Math.cos;
          var a = 0.5 - c((laltitud_dos - laltitud) * p)/2 + 
          c(laltitud * p) * c(laltitud_dos * p) * 
          (1 - c((longitud_dos - longitud) * p))/2;

        return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
    }
    


}).controller("ctrlCotizador",function($scope){
  $scope.opciones={};
    
  $scope.changeMonto=function(){
      $scope.getTotal();
  };
  $scope.changePago=function(){
      $scope.getTotal();
  };
  $scope.changePeriodo=function(){
      $scope.getPeriodoName();
      $scope.getTotal();
  };
  $scope.getTotal=function(){
      $scope.c = new Calculadora($scope.opciones.monto,0,$scope.opciones.no_pagos,$scope.opciones.periodo);
      $scope.opciones.total=$scope.c.getPago();  
  };
  $scope.getPeriodoName=function(){
      switch(parseInt($scope.opciones.periodo)){
          case 1:
              $scope.opciones.periodoName="mensuales";
              break;
          case 2:
               $scope.opciones.periodoName= "quincenales";
              break;
          case 3:
               $scope.opciones.periodoName="semanales";
              break;
      }
  };
$scope.init=function(){
console.log("init");
  $scope.opciones.monto=500;
  $scope.opciones.periodo=1;
  $scope.opciones.no_pagos=10;
  $scope.getPeriodoName();
  $scope.getTotal();
}
  
  
}).controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('controlador', function($scope, $stateParams) {
});

/*
app.controller("ctrl",function($scope, $ionicSideMenuDelegate){
  $scope.monto=500;
  $scope.changeMonto=function(){
      $scope.getTotal();
  };
  $scope.changePago=function(){
      $scope.getTotal();
  };
  $scope.changePeriodo=function(){
      $scope.getPeriodoName();
      $scope.getTotal();
  };
  $scope.getTotal=function(){
      $scope.c = new Calculadora($scope.monto,0,$scope.no_pagos,$scope.periodo);
      $scope.total=$scope.c.getPago();  
  };
  $scope.getPeriodoName=function(){
      switch(parseInt($scope.periodo)){
          case 1:
              $scope.periodoName="mensuales";
              break;
          case 2:
               $scope.periodoName= "quincenales";
              break;
          case 3:
               $scope.periodoName="semanales";
              break;
      }
  };
  
});
*/

